<?php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserCompany", mappedBy="users")
     */
    private $userscompany;

    public function __construct()
    {
        parent::__construct();
        $this->usersCompany = new ArrayCollection();
    }

    /**
     * @return Collection|UserCompany[]
     */
    public function getUsersCompany(): Collection
    {
        return $this->usersCompany;
    }
}