<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class UserController extends Controller
{
    /**
     * @Route("/users", name="users_index")
     */
    public function indexAction(Request $request)
    {
        $user = new User();
        $user->setUsername("Peanut");
        $user->setUsernameCanonical("Peanut");
        $user->setEmail("peanut@ausy-group.com");
        $user->setEmailCanonical("peanut@ausy-group.com");
        $user->setEnabled("active");
        $user->setPassword("333");
        $user->setRoles(["Dev"]);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($user);
        $entityManager->flush();

        return $this->render('user/index.html.twig');
    }

}