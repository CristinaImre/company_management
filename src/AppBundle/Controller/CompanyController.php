<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Company;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class CompanyController extends Controller
{
    /**
     * @Route("/companies", name="companies_index")
     */
    public function indexAction(Request $request){
        $company = new Company();
        $company->setName("Ausy");

        $em = $this->getDoctrine()->getManager();
        $em->persist($company);
        $em->flush();

        $em = $this->getDoctrine()->getManager();
        $companies = $em->getRepository(Company::class)->findAll();
        return $this->render('company/index.html.twig', array(
            'companies' => $companies
        ));
    }
}